FROM php:7.4-fpm-alpine

# Set working directory
WORKDIR /var/www

# Add Build Dependencies
RUN apk add --no-cache --virtual .build-deps  \
    zlib-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    libxml2-dev \
    bzip2-dev \
    libzip-dev \ 
    postgresql-dev

# Install extensions

RUN docker-php-ext-configure zip
RUN docker-php-ext-install pdo pdo_mysql exif pcntl zip
RUN docker-php-ext-configure gd #--with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/
RUN docker-php-ext-install gd

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"


# Expose port 9000 and start php-fpm server
EXPOSE 9000
COPY ./deploy.sh /tmp/deploy.sh
RUN chmod +x /tmp/deploy.sh
ENTRYPOINT ["sh", "/tmp/deploy.sh"]
