import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './vue/routes';
import App from './vue/components/App';

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes: routes
});

new Vue({
    el: '#app',
    router: router,
    components: {
        App
    },
});
