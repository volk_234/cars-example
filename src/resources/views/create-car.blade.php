<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Создание автомобиля</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <style>
        label {
            display: block;
        }

        form > div {
            margin-bottom: 15px;
        }

        .alert-danger ul {
            padding: 0;
            margin: 0;
        }

        .alert-danger {
            background: red;
            color: white;
            border-radius: 3px;
            font-size: 14px;
            display: inline-block;
            padding: 15px;
        }
    </style>
</head>
<body>
@isset($car)
    <form method="POST" action="{{route('cars.update', ['car'=>$car->id])}}">
        @method('put')
        @csrf
        <div>
            <label for="brand">Марка</label>
            <input id="brand" type="text" name="brand" value="{{$car->brand}}">
        </div>
        <div>
            <label for="model">Модель</label>
            <input id="model" type="text" name="model" value="{{$car->model}}">
        </div>
        <div>
            <label for="number">Гос. номер</label>
            <input id="number" type="text" name="number" value="{{$car->number}}">
        </div>
        <div>
            <label for="driver_id">Владелец</label>
            <select name="driver_id">
                @foreach($drivers as $driver)
                    @if($car->driver_id == $driver->id)
                        <option selected
                                value="{{$driver->id}}">{{$driver->last_name}} {{$driver->first_name}} {{$driver->middle_name}}</option>
                    @else
                        <option
                            value="{{$driver->id}}">{{$driver->last_name}} {{$driver->first_name}} {{$driver->middle_name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        @if ($errors->any())
            <div>
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <button type="submit">Отправить форму</button>
    </form>
@else
    <form method="POST" action="{{route('cars.store')}}">
        @csrf
        <div>
            <label for="brand">Марка</label>
            <input id="brand" type="text" name="brand" value="{{old('brand')}}">
        </div>
        <div>
            <label for="model">Модель</label>
            <input id="model" type="text" name="model" value="{{old('model')}}">
        </div>
        <div>
            <label for="number">Гос. номер</label>
            <input id="number" type="text" name="number" value="{{old('number')}}">
        </div>
        <div>
            <label for="driver_id">Владелец</label>
            <select name="driver_id">
                @foreach($drivers as $driver)
                    <option
                        value="{{$driver->id}}">{{$driver->last_name}} {{$driver->first_name}} {{$driver->middle_name}}</option>
                @endforeach
            </select>
        </div>
        @if ($errors->any())
            <div>
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <button type="submit">Отправить форму</button>
    </form>
@endisset
</body>
</html>
