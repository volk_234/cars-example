<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Создание водителя</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <style>
        label {
            display: block;
        }

        form > div {
            margin-bottom: 15px;
        }

        .alert-danger ul {
            padding: 0;
            margin: 0;
        }

        .alert-danger {
            background: red;
            color: white;
            border-radius: 3px;
            font-size: 14px;
            display: inline-block;
            padding: 15px;
        }
    </style>
</head>
<body>
@isset($driver)
    <form method="POST" action="{{route('drivers.update', ['driver'=>$driver->id])}}">
        @method('put')
        @csrf
        <div>
            <label for="last-name">Фамилия</label>
            <input id="last-name" type="text" name="last_name" value="{{$driver->last_name}}">
        </div>
        <div>
            <label for="first-name">Имя</label>
            <input id="first-name" type="text" name="first_name" value="{{$driver->first_name}}">
        </div>
        <div>
            <label for="middle-name">Отчество</label>
            <input id="middle-name" type="text" name="middle_name" value="{{$driver->middle_name}}">
        </div>
        @if ($errors->any())
            <div>
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <button type="submit">Отправить форму</button>
    </form>
@else
    <form method="POST" action="{{route('drivers.store')}}">
        @csrf
        <div>
            <label for="last-name">Фамилия</label>
            <input id="last-name" type="text" name="last_name" value="{{old('last_name')}}">
        </div>
        <div>
            <label for="first-name">Имя</label>
            <input id="first-name" type="text" name="first_name" value="{{old('first_name')}}">
        </div>
        <div>
            <label for="middle-name">Отчество</label>
            <input id="middle-name" type="text" name="middle_name" value="{{old('middle_name')}}">
        </div>
        @if ($errors->any())
            <div>
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <button type="submit">Отправить форму</button>
    </form>
@endisset
</body>
</html>
