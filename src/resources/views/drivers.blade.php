<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Водители</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>
<ol>
    @foreach($drivers as $index => $driver)
        <li>
            <div>
                <strong>Фамилия:</strong> {{$driver->last_name}}
            </div>
            <div>
                <strong>Имя:</strong> {{$driver->first_name}}
            </div>
            <div>
                <strong>Отчество:</strong> {{$driver->middle_name}}
            </div>
            <ul>
                @foreach($driver->cars as $car)
                    <li>{{$car->brand}} {{$car->model}}, г.н. {{$car->number}}</li>
                @endforeach
            </ul>

            <a href="{{route('drivers.edit', ['driver'=>$driver->id])}}">Редактировать водителя</a>
            <a style="color: red;"
               href="{{route('drivers.destroy', ['driver'=>$driver->id])}}">Удалить водителя</a>
        </li>
        {{--        @if($car != $cars->last())--}}
        {{--            <hr>--}}
        {{--        @endif--}}

        {{--        @if($index != $cars->count()-1)--}}
        {{--            <hr>--}}
        {{--        @endif--}}

        @if(!$loop->last)
            <hr>
        @endif
    @endforeach
</ol>
<a href="{{route('drivers.create')}}">Создать нового водителя</a>
</body>
</html>
