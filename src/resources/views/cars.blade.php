<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Автомобили</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>
<ol>
    @foreach($cars as $index => $car)
        <li>
            <div>
                <strong>ID:</strong> {{$car->id}}
            </div>
            <div>
                <strong>Марка:</strong> {{$car->brand}}
            </div>
            <div>
                <strong>Модель:</strong> {{$car->model}}
            </div>
            <div>
                <strong>Гос. номер:</strong> {{$car->number}}
            </div>
            <div>
                <strong>Владелец:</strong> {{$car->driver->last_name}} {{$car->driver->first_name}} {{$car->driver->middle_name}}
            </div>
            <a href="{{route('cars.edit', ['car'=>$car->id])}}">Редактировать автомобиль</a>
            <a style="color: red;"
               href="{{route('cars.destroy', ['car'=>$car->id])}}">Удалить автомобиль</a>
        </li>
        {{--        @if($car != $cars->last())--}}
        {{--            <hr>--}}
        {{--        @endif--}}

        {{--        @if($index != $cars->count()-1)--}}
        {{--            <hr>--}}
        {{--        @endif--}}

        @if(!$loop->last)
            <hr>
        @endif
    @endforeach
</ol>
<a href="{{route('cars.create')}}">Создать новый автомобиль</a>
</body>
</html>
