<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Car;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

Route::resource('cars', 'CarController');
Route::get('/cars/{car}/delete',
    'CarController@destroy')->name('cars.destroy');

Route::resource('drivers', 'DriverController');
Route::get('/drivers/{driver}/delete',
    'DriverController@destroy')->name('drivers.destroy');

//Route::get('/cars', 'CarController@index');
//
//Route::get('/cars/create', 'CarController@create');
//Route::post('/cars/create', 'CarController@store');
//
//Route::get('/cars/{car}/edit', 'CarController@edit');
//Route::put('/cars/{car}', 'CarController@update');
//Route::get('/cars/{car}', 'CarController@show');
//

//Route::get('/', function () {
////    $car = new Car();
////    $car->brand = 'KIA';
////    $car->model = 'ceed';
////    $car->save();
//
////    $data = [
////        'brand' => 'Dodge',
////        'model' => 'Challenger',
////        'number' => 'Т873КУ777',
////    ];
////    Car::create($data);
//
////    $cars = Car::where('model', 'Camry')
////        ->orWhere('model', 'Corolla')->get();
//
////    $cars = Car::where('model', 'Camry')
////        ->orWhere('model', 'Corolla')->update(['brand' => 'TOYOTA']);
//
////    Car::destroy(1);
////    Car::findOrFail(1)->destroy();
//
////    $cars = Car::all();
////    dd(($cars ?? collect())->toArray());
//
////    $cars = Car::all();
////    return view('welcome', ['cars' => $cars]);
//});
