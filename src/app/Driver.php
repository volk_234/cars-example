<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $guarded = ['id'];

//    protected $fillable = ['brand', 'number', 'model'];

    public function cars()
    {
        return $this->hasMany(Car::class);
    }
}
