<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded = ['id'];

//    protected $fillable = ['brand', 'number', 'model'];

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
}
