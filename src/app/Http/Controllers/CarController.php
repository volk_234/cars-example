<?php


namespace App\Http\Controllers;


use App\Car;
use App\Driver;
use App\Http\Requests\StoreCarRequest;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function index(Request $request)
    {
        $cars = Car::all();
        return view('cars', ['cars' => $cars]);
    }

    public function create(Request $request)
    {
        $drivers = Driver::all();
        return view('create-car', ['drivers' => $drivers]);
    }

    public function store(StoreCarRequest $request)
    {
        Car::create($request->all());

        return redirect(route('cars.index'));
    }

    public function edit(Request $request, Car $car)
    {
        $drivers = Driver::all();
        return view('create-car', [
            'car' => $car,
            'drivers' => $drivers,
        ]);
    }

    public function update(Request $request, Car $car)
    {
        $this->validate($request, [
            'model' => 'required',
            'brand' => 'required',
            'number' => 'nullable|string|max:9|unique:cars,number,' . $car->id,
        ]);

        $car->update($request->all());

        return redirect(route('cars.index'));
    }

    public function destroy(Request $request, Car $car)
    {
        $car->delete();

        return redirect(route('cars.index'));
    }
}
