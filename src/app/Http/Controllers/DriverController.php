<?php


namespace App\Http\Controllers;


use App\Car;
use App\Driver;
use App\Http\Requests\StoreCarRequest;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function index(Request $request)
    {
        $drivers = Driver::all();
        return view('drivers', ['drivers' => $drivers]);
    }

    public function create(Request $request)
    {
        return view('create-driver');
    }

    public function store(Request $request)
    {
        Driver::create($request->all());

        return redirect(route('drivers.index'));
    }

    public function edit(Request $request, Driver $driver)
    {
        return view('create-driver', ['driver' => $driver]);
    }

    public function update(Request $request, Driver $driver)
    {
//        $this->validate($request, [
//            'model' => 'required',
//            'brand' => 'required',
//            'number' => 'nullable|string|max:9|unique:cars,number,' . $car->id,
//        ]);

        $driver->update($request->all());

        return redirect(route('drivers.index'));
    }

    public function destroy(Request $request, Driver $driver)
    {
        $driver->delete();

        return redirect(route('drivers.index'));
    }
}
